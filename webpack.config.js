const path = require('path');

module.exports = {
  webpack: (config, {
    // eslint-disable-next-line no-unused-vars
    buildId, dev, isServer, defaultLoaders, webpack,
  }) => {
    // Note: we provide webpack above so you should not `require` it
    // Perform customizations to webpack config
    // Important: return the modified config
    config.plugins.push(new webpack.IgnorePlugin(/\/__tests__\//));
    // eslint-disable-next-line no-param-reassign
    config.resolve = {
      modules: [path.resolve(__dirname)],
      extensions: ['.js', '.json', '.jsx', '.css', '.less'],
      alias: {
        react: path.resolve(__dirname, '.', 'node_modules', 'react'),
      },
    };

    return config;
  },
  webpackDevMiddleware: (config) =>
  // Perform customizations to webpack dev middleware config
  // Important: return the modified config

    // eslint-disable-next-line implicit-arrow-linebreak
    config
  ,
};
