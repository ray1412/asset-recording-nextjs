import _ from 'lodash';

export const safeAppend = (urlA, urlB) => {
  const sanitizedUrlA = _.trimEnd(urlA.trim(), '/');
  const sanitizedUrlB = _.trimStart(urlB.trim(), '/');

  return `${sanitizedUrlA}/${sanitizedUrlB}`;
};

export const assetPrefix = (assetPublicFilePath) => safeAppend(
  process.env.ASSET_PREFIX,
  assetPublicFilePath,
);

export const basePath = (href) => safeAppend(
  process.env.BASE_PATH,
  href,
);

export const demeterWebBasePath = (href) => safeAppend(
  process.env.DEMETER_WEB_BASE_PATH,
  href,
);
