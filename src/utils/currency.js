import _ from 'lodash';

const thousandSeparatorRegex = /\B(?=(\d{3})+(?!\d))/g;

/**
 *
 * @param {Number} number
 * @param {String} prefix
 * @param {Number} round
 * @param {Boolean} floor
 * @param {String} thousandSeparator
 * @returns {string}
 */
const format = ({
  number = 0,
  prefix = 'Rp',
  round,
  floor = true,
  thousandSeparator = ',',
}) => {
  // Replace leading zero
  let returnedNumber = Number(number);

  if (_.isNaN(number)) {
    returnedNumber = 0;
  }

  const returnedPrefix = prefix;

  if (round) {
    returnedNumber = _.round(returnedNumber, round || 0);
  }

  /**
   * Since we are able to retrieve comma(.)
   */
  if (floor) {
    returnedNumber = Math.floor(returnedNumber);
  }

  /**
   * Since we are able to retrieve comma(.), then I change the thousandSeparator into ','
   */
  return `${returnedPrefix} ${returnedNumber
    .toString()
    .replace(thousandSeparatorRegex, thousandSeparator)}`.trim();
};

/**
 *
 * @param {String} value
 * @param {String} prefix
 * @returns {number}
 */
const getCurrencyRealNumber = ({ value, prefix = 'Rp' }) => {
  if (!value) {
    return 0;
  }

  const number = Number(value.replace(/[^0-9]/g, '').replace(prefix, '').trim());

  if (_.isNaN(number)) {
    return 0;
  }

  return number;
};

export default {
  format,
  getCurrencyRealNumber,
};
