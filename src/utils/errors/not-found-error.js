function NotFoundError(message, options) {
  const { method, url } = options.response.config;

  this.message = message;
  this.name = 'NotFoundError';
  this.status = 404;
  this.response = {
    type: 'NotFoundError',
    message: `${message}\nTarget: [${method}] ${url}`,
  };

  if (options.stack) {
    this.stack = options.stack;
  } else {
    Error.captureStackTrace(this, NotFoundError);
  }
}

NotFoundError.prototype = Object.create(Error.prototype);
NotFoundError.prototype.constructor = NotFoundError;

module.exports = NotFoundError;
