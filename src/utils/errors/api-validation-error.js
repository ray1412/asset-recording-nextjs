import _ from 'lodash';

/**
 * A class that represents validation error 422
 *
 * @constructor
 * @author Theo Pratama <tpratama@cermati.com>
 * @param {String} message - the error message
 * @param {Object} options
 */
function ApiValidationError(message, options) {
  const _options = _.defaults(options, {
    response: message,
  });
  const { method, url } = _options.response.config;

  this.message = `${message}\nTarget: [${method}] ${url}`;
  this.status = 422;
  this.name = 'ApiValidationError';
  this.response = _options.response;

  if (_options.stack) {
    this.stack = _options.stack;
  } else {
    Error.captureStackTrace(this, ApiValidationError);
  }
}

ApiValidationError.prototype = Object.create(Error.prototype);
ApiValidationError.prototype.constructor = ApiValidationError;

export default ApiValidationError;
