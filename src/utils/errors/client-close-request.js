/**
 * A class that represents cancellation error
 * Error code reference: https://stackoverflow.com/a/46361806/13819838
 *
 * @param message
 * @constructor
 */

function ClientCloseRequest(message) {
  this.message = message;
  this.name = 'Cancelled';
  this.status = 499;
  this.response = {
    type: 'Cancelled',
    message,
  };
}

ClientCloseRequest.prototype = Object.create(Error.prototype);
ClientCloseRequest.prototype.CancelError = ClientCloseRequest;

module.exports = ClientCloseRequest;
