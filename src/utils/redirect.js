import Router from 'next/router';
import { basePath } from 'src/utils/url';

/**
 * Universal redirect that works on client and server side
 *
 * @param {Object} config
 * @param {Express.Response} config.res
 * @param {String} config.href
 * @param {String} config.as displayed url if on client side
 * @returns {Boolean}
 */
export default function redirect(config) {
  const { res, href, as = basePath(href) } = config;
  const isServer = res && typeof window === 'undefined';

  if (isServer) {
    res.writeHead(302, {
      Location: as || href,
    });

    return res.end();
  }

  return Router.push(href, as);
}
