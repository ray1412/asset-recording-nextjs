const DEFAULT_URL = 'https://accounts.google.com/o/oauth2/v2/auth'
const DEFAULT_QUERY = {
  redirect_uri: process.env.NEXT_PUBLIC_GOOGLE_OAUTH_REDIRECT_URL,
  client_id: process.env.NEXT_PUBLIC_GOOGLE_CLIENT_ID,
  access_type: 'offline',
  response_type: 'code',
  prompt: 'consent',
  scope: [
    'https://www.googleapis.com/auth/userinfo.profile',
    'https://www.googleapis.com/auth/userinfo.email'
  ].join(" ")
}
const getGoogleOAuthUrl = () => {
  const qs = new URLSearchParams(DEFAULT_QUERY)
  
  console.log(`${DEFAULT_URL}?${qs.toString()}`)
  return `${DEFAULT_URL}?${qs.toString()}`
}

export default getGoogleOAuthUrl;