import _ from 'lodash'

import { isJSON } from 'src/utils/object';
import storageUtil from 'src/utils/storage';

const userIdentifierKey = 'loggedInUser';

/**
 * Get logged-in user.
 * @author Raymond Haryanto
 * @returns {Object} User
 */
export const getUser = () => {
  const serializedUser = storageUtil.get(userIdentifierKey);
  
  if (_.isEmpty(serializedUser) && !isJSON(serializedUser)) {
    /**
     * Return empty object if key is not exist or has empty value
     */
    return {};
  }
  
  return JSON.parse(serializedUser);
};

/**
 * Set User Info.
 * @author Raymond Haryanto
 * @param {Object} user
 * @returns {void}
 */
export const setUser = (user) => {
  return storageUtil.set(userIdentifierKey, JSON.stringify(user));
};

/**
 * @author Raymond Haryanto
 * @returns {void}
 */
export const removeUser = () => {
  return storageUtil.remove(userIdentifierKey);
};
