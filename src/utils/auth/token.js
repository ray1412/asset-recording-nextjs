import storageUtil from 'src/utils/storage';

const accessTokenKey = 'accessToken';

/**
 * Set user token.
 *
 * @param {String} token
 * @author Raymond Haryanto
 * @returns {void}
 */
export const setToken = (token) => {
  return storageUtil.set(accessTokenKey);
};

/**
 * Get User Token from storage or cookie
 *
 * @author Raymond Haryanto
 * @returns {String} token
 */
export const getToken = () => {
  return storageUtil.get(accessTokenKey);
};

/**
 * Remove token from localStorage and cookie
 *
 * @author Raymond Haryanto
 * @returns {void}
 */
/**
 *
 */
export const clearToken = () => {
  return storageUtil.remove(accessTokenKey);
}
