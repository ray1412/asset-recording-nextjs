import axios from 'axios';
import httpContext from 'express-http-context';
import _ from 'lodash';

import apiConfig from 'src/config/api';
import { getToken } from 'src/utils/auth/token';
import ApiValidationError from 'src/utils/errors/api-validation-error';
import BadGatewayError from 'src/utils/errors/bad-gateway-error';
import ClientCloseRequest from 'src/utils/errors/client-close-request';
import ForbiddenError from 'src/utils/errors/forbidden-error';
import NotFoundError from 'src/utils/errors/not-found-error';
import ResourceNotFound from 'src/utils/errors/resource-not-found-error';
import ServiceUnavailableError from 'src/utils/errors/service-unavailable-error';


export const handleError = (error) => {
  const { response, stack, message } = error;
  const status = _.get(response, 'status', null);
  const { message: errorMessageFromBE, type } = _.get(response, 'data.error', {});
  
  // Debug for checking unexpected tiket API error (T65274)
  if (
    typeof window !== 'undefined'
    && _.endsWith(_.toLower(window.location.pathname), '/whitelabel/tiket/credit-limit/home/dashboard')
  ) {
    Sentry.captureException(new TiketApiError(message, { response, stack, error }));
  }
  
  // Network error
  // Handled in watch-any-action redux middleware by displaying offline page for "network error" messages
  // We can allow some whitelist by customizing the error message
  if (!response) {
    // Whitelist AB Testing network error
    const { GET } = apiConfig;
    if (_.get(error, 'config.url') === GET.ab.getVariation()) {
      throw new Error('Failed to get AB Testing response');
    }
    
    // Whitelist axios cancellation error
    if (axios.isCancel(error)) {
      throw new ClientCloseRequest(message);
    }
  }
  
  switch (status) {
    case 422: {
      throw new ApiValidationError(message, {
        response,
        stack,
      });
    }
    case 403: {
      throw new ForbiddenError('Invalid token', {
        response,
        stack,
      });
    }
    case 404: {
      if (type === 'NotFoundError') {
        throw new NotFoundError(errorMessageFromBE, {
          response,
          stack,
        });
      }
      
      throw new ResourceNotFound('Please check your API URL', {
        response,
        stack,
      });
    }
    case 502: {
      throw new BadGatewayError('Server Not Responding', {
        response,
        stack,
      });
    }
    case 503: {
      throw new ServiceUnavailableError('Server unable to handle request', {
        response,
        stack,
      });
    }
    default: {
      throw error;
    }
  }
};

// Global cancel token to cancel all ongoing API request on client
let cancelAllRequestsTokenSource = typeof window !== 'undefined'
  ? axios.CancelToken.source()
  : null;

/**
 * Cancel all ongoing request
 * Should only be called on client
 *
 * @author Edwin Tandiono <etandiono@indodana.com>
 * @param message
 */
export const cancelAllRequest = (message) => {
  if (typeof window !== 'undefined') {
    cancelAllRequestsTokenSource.cancel(message);
    
    // Renew source (if we don't renew it, previously cancelled request can't be used again)
    cancelAllRequestsTokenSource = axios.CancelToken.source();
  }
};

const createApiDefaultConfig = () => {
  // Client default config
  if (typeof window !== 'undefined') {
    return ({
      cancelToken: cancelAllRequestsTokenSource.token,
    });
  }
  
  // Server default config
  return {};
};

class ApiCaller {
  constructor(api) {
    this.api = api;
  }
  
  get(url, options = {}) {
    return Bluebird.resolve()
      .then(() => {
        const { data, ...otherConfig } = options;
        const config = {
          ...createApiDefaultConfig(),
          ...otherConfig,
          ...data,
          timeout: _.get(data, 'timeout', 0),
        };
        
        return this.api.get(url, config);
      })
      .catch(handleError)
      .catch(ApiValidationError, (error) => throw error)
      .catch(ResourceNotFound, (error) => throw error)
      .catch(BadGatewayError, (error) => throw error)
      .catch(ServiceUnavailableError, (error) => throw error)
      .catch(ClientCloseRequest, (error) => throw error);
  }
  
  post(url, options = {}) {
    return Bluebird.resolve()
      .then(() => {
        const config = {
          ...createApiDefaultConfig(),
          ...options.config,
          timeout: _.get(options, 'config.timeout', 0),
        };
        return this.api.post(url, options.data, config);
      })
      .catch(handleError)
      .catch(ApiValidationError, (error) => throw error)
      .catch(ResourceNotFound, (error) => throw error)
      .catch(BadGatewayError, (error) => throw error)
      .catch(ServiceUnavailableError, (error) => throw error)
      .catch(ClientCloseRequest, (error) => throw error);
  }
  
  patch(url, options = {}) {
    return Bluebird.resolve()
      .then(() => {
        const config = {
          ...createApiDefaultConfig(),
          ...options.config,
          timeout: _.get(options, 'config.timeout', 0),
        };
        return this.api.patch(url, options.data, config);
      })
      .catch(handleError)
      .catch(ApiValidationError, (error) => throw error)
      .catch(ResourceNotFound, (error) => throw error)
      .catch(BadGatewayError, (error) => throw error)
      .catch(ServiceUnavailableError, (error) => throw error)
      .catch(ClientCloseRequest, (error) => throw error);
  }
  
  delete(url, options = {}) {
    return Bluebird.resolve()
      .then(() => this.api.delete(url, {
        ...createApiDefaultConfig(),
        data: options.data,
      }))
      .catch(handleError)
      .catch(ApiValidationError, (error) => throw error)
      .catch(ResourceNotFound, (error) => throw error)
      .catch(BadGatewayError, (error) => throw error)
      .catch(ServiceUnavailableError, (error) => throw error)
      .catch(ClientCloseRequest, (error) => throw error);
  }
  
  put(url, options = {}) {
    return Bluebird.resolve()
      .then(() => {
        const config = {
          ...createApiDefaultConfig(),
          ...options.config,
          timeout: _.get(options, 'config.timeout', 0),
        };
        return this.api.put(url, options.data, config);
      })
      .catch(handleError)
      .catch(ApiValidationError, (error) => throw error)
      .catch(ResourceNotFound, (error) => throw error)
      .catch(BadGatewayError, (error) => throw error)
      .catch(ServiceUnavailableError, (error) => throw error)
      .catch(ClientCloseRequest, (error) => throw error);
  }
}
/**
 * @return {ApiCaller}
 */
const ApiCall = () => {
  const generalAxiosConfig = {};
  const headers = {};
  const bearerToken = getToken()
  
  if (bearerToken) {
    _.assign(headers, { Authorization: `Bearer ${bearerToken}` });
  }
  
  // Add client IP to headers for server side fetching
  if (typeof window === 'undefined' && httpContext.get('x-client-ip')) {
    _.assign(headers, { 'x-client-ip': httpContext.get('x-client-ip') });
  }
  
  if (!_.isEmpty(headers)) {
    _.assign(generalAxiosConfig, { headers });
  }
  
  return new ApiCaller(axios.create(generalAxiosConfig));
};

export default ApiCall;

export const extractData = _.property('data.data');
