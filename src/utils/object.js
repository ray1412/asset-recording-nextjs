/**
 * Check if a string can be parsed to JSON object
 * @param {String} str
 * @return {Boolean}
 */
export const isJSON = (str) => {
  if (typeof (str) !== 'string') {
    return false;
  }
  
  try {
    JSON.parse(str);
    
    return true;
  } catch (e) {
    return false;
  }
};
