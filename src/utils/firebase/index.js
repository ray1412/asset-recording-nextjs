import dayjs from 'dayjs';
import _ from 'lodash';
import storageUtil from 'utils/storage';

const firebase = require('firebase/app').default;
require('firebase/auth');
require('firebase/database');
require('firebase/analytics');

/**
 * TODO move `apiKey` to heroku env
 * */
const firebaseConfig = {
  apiKey: 'AIzaSyABEa_g2P_ud24xa4jQZ3spdiSuiekncVg',
  authDomain: 'asset-recording-a9b8b.firebaseapp.com',
  databaseURL: 'https://asset-recording-a9b8b.firebaseio.com',
  projectId: 'asset-recording-a9b8b',
  storageUtilBucket: 'asset-recording-a9b8b.appspot.com',
  messagingSenderId: '278290837366',
  appId: '1:278290837366:web:3b1c7fee78337cc5855457',
  measurementId: 'G-NTRFZ7K6J7',
};

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
  // Get a reference to the database service
  firebase.database().ref('assets');
}

/**
 *
 * @param {String} email
 * @param {String} password
 * @returns {Promise<firebase.auth.UserCredential>}
 */
const doFirebaseLogin = ({ email, password }) => firebase.auth().signInWithEmailAndPassword(email, password);

const doFirebaseLogout = () => firebase.auth().signOut();

/**
 *
 * @param {Function} callback
 * @returns {firebase.Unsubscribe}
 */
const doVerifyUser = (callback) => firebase.auth().onAuthStateChanged((userAuth) => callback(userAuth));

const getParticularAsset = (assetId) => {
  try {
    const userCredential = JSON.parse(storageUtil.get());
    const constructedPath = `${userCredential.uid}-assets/${assetId}`;

    return firebase.database()
      .ref(constructedPath)
      .once('value')
      .then((data) => data.val());
  } catch (e) {
    return new Promise((resolve, reject) => {
      reject(e);
    });
  }
};

/**
 * @returns {Promise<firebase.database.DataSnapshot>}
 */
const getAllAsset = ({ req }) => {
  try {
    const userCredential = storageUtil.get(storageUtil.userCredentialKey, { req });
    
    const constructedPath = `${userCredential.uid}-assets`;
    
    // TODO change it to BE
    return firebase.database()
      .ref(constructedPath)
      .once('value')
      .then((data) => data.val());
  } catch (e) {
    return new Promise((resolve, reject) => {
      reject(e);
    });
  }
};

/**
 *
 * @param {Number} currentAmount
 * @param {Number} investmentReturnPerYear
 * @param {String} investmentType
 * @param {String} name
 * @param {Number} targetAmount
 * @param {String} description
 * @returns {Promise<firebase.database.DataSnapshot>}
 */
const addAsset = ({
  currentAmount,
  investmentReturnPerYear,
  investmentType,
  name,
  targetAmount,
  description,
} = {}) => {
  const today = dayjs().unix().toString();

  const payload = {
    currentAmount,
    investmentReturnPerYear,
    investmentType,
    name,
    targetAmount,
    description,
  };

  const enhancedPayload = {
    ...payload,
    createdAt: today,
    updatedAt: today,
    activityLog: {
      [today]: {
        ...payload,
      },
    },
  };

  const userCredential = JSON.parse(storageUtil.get());
  const constructedPath = `${userCredential.uid}-assets`;

  return firebase.database().ref(constructedPath).push(enhancedPayload).then((data) => {
    const { key } = data;
    console.log('success add asset key ==> ', key);

    return key;
  });
};

/**
 *
 * @param {String} assetId
 * @param {Number} currentAmount
 * @param {Number} investmentReturnPerYear
 * @param {String} investmentType
 * @param {String} name
 * @param {Number} targetAmount
 * @param {String} description
 * @param {String} createdAt - Unix Timestamp in seconds
 * @param {Object} activityLog
 * @param {Function} callBack
 * @returns {Promise<any>}
 */
const updateAsset = ({
  assetId,
  currentAmount,
  investmentReturnPerYear,
  investmentType,
  name,
  targetAmount,
  description,
  createdAt,
  activityLog,
  callBack,
}) => {
  const today = dayjs().unix().toString();

  const payload = {
    currentAmount,
    investmentReturnPerYear,
    investmentType,
    name,
    targetAmount,
    description,
  };

  const newActivityLog = {
    ...activityLog,
    [today]: {
      ...payload,
    },
  };

  const enhancedPayload = {
    ...payload,
    createdAt: !_.isEmpty(createdAt) ? createdAt : today,
    updatedAt: today,
    activityLog: { ...newActivityLog },
  };

  const userCredential = JSON.parse(storageUtil.get());
  const constructedPath = `${userCredential.uid}-assets/${assetId}`;

  return firebase.database().ref(constructedPath).set(
    enhancedPayload,
    (error) => callBack(error, constructedPath),
  );
};

/**
 *
 * @param {String} assetId
 * @returns {Promise<any>}
 */
const removeAsset = (assetId) => {
  const userCredential = JSON.parse(storageUtil.get());
  const constructedPath = `${userCredential.uid}-assets/${assetId}`;

  return firebase.database().ref(constructedPath).remove();
};

const loginWithGoogleAuthProvider = () => {
  return firebase
    .auth()
    .signInWithPopup(new firebase.auth.GoogleAuthProvider())
    .then((userCredential) => {
      console.log({ userCredential })
    })
}

export {
  firebase,
  doFirebaseLogin,
  doFirebaseLogout,
  doVerifyUser,
  getParticularAsset,
  getAllAsset,
  addAsset,
  updateAsset,
  removeAsset,
  loginWithGoogleAuthProvider
};
