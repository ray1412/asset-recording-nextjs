import _ from 'lodash';
import Cookies from 'universal-cookie';

import { COOKIES_CONFIG } from 'src/constant/cookies';

const cookies = new Cookies();

const userCredentialKey = 'am-ray-credential';

/**
 * Set date into storage.
 *
 * @param {String} key
 * @param {*} value
 * @returns {void}
 */

/**
 *
 * @param {String} key
 * @param {*} value
 * @param {Object} options
 * @param {String} options.storageType
 * */
const set = (
  key = userCredentialKey,
  value,
  { storageType = 'localStorage' } = {},
) => {
  if (typeof window !== 'undefined') {
    switch (storageType) {
      case 'localStorage': {
        window.localStorage.setItem(key, value);
        break;
      }
      case 'sessionStorage': {
        window.sessionStorage.setItem(key, value);
        break;
      }
      default: {
        break;
      }
    }
  }

  if (cookies) {
    cookies.set(key, value, COOKIES_CONFIG);
  }
};

/**
 * Get a value from storage / cookies
 * @param {String} key
 * @param {Object} options
 * @param {Object} options.req
 * @param {String} options.storageType
 * @returns {string}
 */
const get = (
  key = userCredentialKey,
  {
    req,
    storageType = 'localStorage',
  } = {},
) => {
  let value;

  if (typeof window !== 'undefined') {
    switch (storageType) {
      case 'localStorage': {
        value = window.localStorage.getItem(key);
        break;
      }
      case 'sessionStorage': {
        value = window.sessionStorage.getItem(key);
        break;
      }
      default: {
        value = null;
        break;
      }
    }
  }

  if (_.isUndefined(value) && cookies) {
    if (req) {
      value = JSON.parse(_.get(req, ['cookies', key]));
    } else {
      value = cookies.get(key);
    }
  }

  return value;
};

/**
 * Remove data in storage and cookies.
 * @param {String} key
 * @param {Object} options
 * @param {String} options.storageType
 * @returns {void}
 */
const remove = (
  key,
  { storageType = 'localStorage' } = {},
) => {
  if (typeof window !== 'undefined') {
    switch (storageType) {
      case 'localStorage': {
        window.localStorage.removeItem(key);
        break;
      }
      case 'sessionStorage': {
        window.sessionStorage.removeItem(key);
        break;
      }
      default: {
        break;
      }
    }
  }

  if (cookies) {
    cookies.remove(key, COOKIES_CONFIG);
  }
};

/**
 * Check if the given key exists in storage.
 *
 * @param {String} key
 * @param {Object} options
 * @param {String} options.storageType
 * @returns {Boolean}
 */
const has = (
  key,
  { storageType = 'localStorage' } = {},
) => {
  let value;

  if (typeof window !== 'undefined') {
    switch (storageType) {
      case 'localStorage': {
        value = window.localStorage.getItem(key);
        break;
      }
      case 'sessionStorage': {
        value = window.sessionStorage.getItem(key);
        break;
      }
      default: {
        break;
      }
    }
  }

  if (_.isUndefined(value) && cookies) {
    value = cookies.get(key);
  }

  return !_.isUndefined(value);
};

/**
 * Clear all key in storage and cookie
 *
 * @params {void}
 * @returns {void}
 */
const clear = () => {
  if (typeof window !== 'undefined') {
    window.localStorage.clear();
    window.sessionStorage.clear();
  }
  
  if (cookies) {
    const cookieContent = cookies.getAll();
    
    _.each(_.keys(cookieContent), (key) => cookies.remove(key, {path: '/'}));
  }
};


export default {
  set,
  get,
  remove,
  has,
  clear,
  userCredentialKey,
};
