const _ = require('lodash');
const queryString = require('querystring');

const routes = {
  // REDUX_TESTER_PAGE: {
  //   name: 'test-redux',
  //   page: '/test-redux',
  //   fileLocation: '/redux-tester',
  // },
  // TEST_FILM_PAGE: {
  //   name: 'test-film',
  //   page: '/films/list',
  //   fileLocation: '/films',
  // },
  // TEST_FILM_DETAIL_PAGE: {
  //   name: 'test-film-detail',
  //   page: '/films/list/[id]',
  //   fileLocation: '/films/:id',
  // },
  ASSET_MANAGEMENT_HOME_PAGE: {
    name: 'Asset Management',
    page: '/asset-management',
    fileLocation: '/asset-management',
  },
  ASSET_MANAGEMENT_LOGIN_PAGE: {
    name: 'Asset Management Login',
    page: '/asset-management/login',
    fileLocation: '/asset-management/login',
  },
  ASSET_MANAGEMENT_DASHBOARD_PAGE: {
    name: 'Asset Management Dashboard',
    page: '/asset-management/dashboard',
    fileLocation: '/asset-management/dashboard',
  },
  ASSET_MANAGEMENT_DASHBOARD_LIST_PAGE: {
    name: 'Asset Management Dashboard List',
    page: '/asset-management/dashboard/list',
    fileLocation: '/asset-management/dashboard/list',
  },
  ASSET_MANAGEMENT_DASHBOARD_DETAIL_PAGE: {
    name: 'Asset Management Dashboard Detail',
    page: '/asset-management/dashboard/list/[id]',
    fileLocation: '/asset-management/dashboard/list/[id]',
  },
  ASSET_MANAGEMENT_DASHBOARD_CREATE_PAGE: {
    name: 'Asset Management Dashboard Create',
    page: '/asset-management/dashboard/create',
    fileLocation: '/asset-management/dashboard/create',
  },
  ASSET_MANAGEMENT_DASHBOARD_EDIT_PAGE: {
    name: 'Asset Management Dashboard Create',
    page: '/asset-management/dashboard/edit/[id]',
    fileLocation: '/asset-management/dashboard/edit/[id]',
  },
};

const pathBuilder = ({ path, paramObj }) => {
  const fullPath = path;

  if (!_.isObject(paramObj)) {
    return fullPath;
  }

  const mutableParams = paramObj;
  const pathWithParams = _.keys(paramObj).reduce(
    (acc, paramKey) => {
      const newPath = acc.replace(`[${paramKey}]`, paramObj[paramKey]);

      if (acc !== newPath) {
        delete mutableParams[paramKey];
      }

      return newPath;
    },
    fullPath,
  );

  const stringifiedQueryParam = !_.isEmpty(mutableParams)
    ? `?${queryString.stringify(mutableParams)}`
    : '';

  return `${pathWithParams}${stringifiedQueryParam}`;
};

export {
  routes,
  pathBuilder,
};
