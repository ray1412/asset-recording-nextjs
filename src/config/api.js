import _ from "lodash";

import logger from 'src/utils/logger';

/**
 * Create url builder
 *
 * @author Raymond Haryanto
 * @param {String} baseUrl
 * @param {String} path
 * @returns {Function} url builder
 */
const urlBuilder = (baseUrl, path) => (paramObj) => {
  const isServer = typeof window === 'undefined';
  
  if (
    isServer
    && _.startsWith(baseUrl, 'https')
    && process.env.NODE_ENV === 'production'
  ) {
    // Calling public API from server side will cause the API to not
    // use the client IP set on x-client-ip header. This could potentially trigger
    // DoS blacklisting of our own server if the API has rate limiter.
    logger.error(`[API] Server side API call is initiated with public URL. baseUrl: ${baseUrl}, path: ${path}`);
  }
  
  const fullPath = `${baseUrl}${path}`;
  
  if (!_.isObject(paramObj)) {
    return fullPath;
  }
  
  return _.keys(paramObj)
    .reduce(
      (acc, paramKey) => acc.replace(`:${paramKey}`, paramObj[paramKey]),
      fullPath,
    );
};
const backendApiUrlBuilder = (path) => urlBuilder(process.env.BACKEND_API_URL, path)

export default {
  POST: {
  
  },
  PATCH: {
  
  },
  GET: {
  
  
  },
  PUT: {
  
  },
};
