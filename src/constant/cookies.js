// Using ES5 exports due to this file being imported on server modules
exports.COOKIES_CONFIG = {
  httpOnly: false,
  path: process.env.BASE_PATH,
  maxAge: 604800, // 1 week (in seconds)
};
