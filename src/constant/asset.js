const INVESTMENT_TYPE_ENUM = {
  REKSADANA_PASAR_UANG: {
    value: 'REKSADANA_PASAR_UANG',
    text: 'Reksadana Pasar Uang',
  },
  REKSADANA_OBLIGASI: {
    value: 'REKSADANA_OBLIGASI',
    text: 'Reksadana Obligasi',
  },
  REKSADANA_CAMPURAN: {
    value: 'REKSADANA_CAMPURAN',
    text: 'Reksadana Campuran',
  },
  REKSADANA_SAHAM: {
    value: 'REKSADANA_SAHAM',
    text: 'Reksadana Saham',
  },
  DEPOSITO: {
    value: 'DEPOSITO',
    text: 'Deposito',
  },
  SAHAM: {
    value: 'SAHAM',
    text: 'Saham',
  },
  P2P: {
    value: 'P2P',
    text: 'P2P Lending',
  },
  EMAS: {
    value: 'EMAS',
    text: 'Emas',
  },
  BANK: {
    value: 'BANK',
    text: 'Tabungan Bank',
  },
};

const SAVING_TYPE = {
  MARRIAGE: {
    value: 'MARRIAGE-SAVING',
    text: 'Tabungan Pernikahan',
  },
  NORMAL_SAVING: {
    value: 'NORMAL-SAVING',
    text: 'Tabungan Senang2',
  },
  TRAVEL: {
    value: 'TRAVEL-SAVING',
    text: 'Tabungan Travelling',
  },
  BIRTH_CHILD: {
    value: 'BIRTH-CHILD-SAVING',
    text: 'Tabungan Maternity',
  },
  EDUCATION_CHILD: {
    value: 'EDUCATION-CHILD-SAVING',
    text: 'Tabungan Pendidikan Anak',
  },
  VEHICLE: {
    value: 'VEHICLE-SAVING',
    text: 'Tabungan Kendaraan',
  },
  HOME: {
    value: 'HOME-SAVING',
    text: 'Tabungan Rumah',
  },
  RETIREMENT: {
    value: 'RETIREMENT-SAVING',
    text: 'Tabungan Pensiun',
  },
  EMERGENCY: {
    value: 'EMERGENCY-SAVING',
    text: 'Tabungan Darurat',
  },
  HOUSEHOLD: {
    value: 'HOUSEHOLD-SAVING',
    text: 'Tabungan Keperluan Rumah',
  },
};

const INVESTMENT_TABLE_HEADER_MAPPING = {
  assetId: 'ID',
  name: 'Name',
  investmentType: 'Jenis Investasi',
  currentAmount: 'Nilai Asset',
  targetAmount: 'Target',
  investmentReturnPerYear: 'Return per Tahun (%)',
  description: 'Deskripsi',
  action: 'Action',
};

export {
  INVESTMENT_TYPE_ENUM,
  SAVING_TYPE,
  INVESTMENT_TABLE_HEADER_MAPPING,
};
