const listOfFeatures = [
  {
    key: 'assetManagement',
    text: 'Asset Management',
    targetUrl: '/asset-management',
  },
  {
    key: 'portfolio',
    text: 'Portfolio',
    targetUrl: '/portfolio',
  },
];
export default listOfFeatures;
