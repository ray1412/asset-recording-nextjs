export const ADVANCED_SKILLS = [
  'HTML5',
  'CSS3',
  'SASS',
  'LESS',
  'JSON',
  'JavaScript',
  'JQuery',
  'React',
  'React Native',
  'Webpack',
  'NextJS'
];

export const INTERMEDIATE_SKILLS = [
  'Isomorphic Web Application Architecture',
  'Web and Image optimization',
  'GCP especially Google Cloud Storage and Firebase',
  'CI/CD',
  'Load Balancer such as NGINX',
  'Java Web Application',
  'other modern JS Frameworks',
];
