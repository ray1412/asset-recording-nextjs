require('dotenv').config();

const express = require('express');
const next = require('next');
const cookiesMiddleware = require('universal-cookie-express');

const dev = process.env.NODE_ENV !== 'production';
const port = process.env.NODE_ENV !== 'production' ? process.env.DEV_PORT : process.env.PROD_PORT;
const app = next({
  dev,
});
const handle = app.getRequestHandler();

app.prepare().then(() => {
  const server = express();
  server.use(cookiesMiddleware());
  server.all('*', (req, res) => handle(req, res));

  server.listen(port, (err) => {
    if (err) throw err;
    // eslint-disable-next-line no-console
    console.log(`> Ready on http://localhost:${port}`);
  });
});
