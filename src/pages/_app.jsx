import React, { useEffect } from 'react';
import Head from 'next/head';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';
import Router from 'next/router';
import NProgress from 'nprogress';

import 'antd/dist/antd.css';
import 'react-super-responsive-table/dist/SuperResponsiveTableStyle.css';
import 'style/index.scss';
import { useStore } from 'src/redux/store';

function App({ Component, pageProps }) {
  const store = useStore(pageProps.initialReduxState);

  NProgress.configure({ showSpinner: false });

  useEffect(() => {
    Router.events.on('routeChangeStart', () => {
      NProgress.start();
    });

    Router.events.on('routeChangeComplete', () => {
      NProgress.done();
    });

    Router.events.on('routeChangeError', () => {
      NProgress.done();
    });

    if ('serviceWorker' in navigator) {
      window.addEventListener('load', () => {
        navigator.serviceWorker.register('/sw.js').then(
          (registration) => {
            console.log('Service Worker registration successful with scope: ', registration.scope);
          },
          (err) => {
            console.log('Service Worker registration failed: ', err);
          },
        );
      });
    }
  }, []);

  return (
    <>
      <Head>
        <title>Raymond Haryanto - All in One App</title>
      </Head>
      <Provider store={store}>
        <Component {...pageProps} />
      </Provider>
    </>
  );
}

App.propTypes = {
  Component: PropTypes.instanceOf(Object).isRequired,
  pageProps: PropTypes.instanceOf(Object).isRequired,
};

export default App;
