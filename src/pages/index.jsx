import { Button } from 'antd';
import { useRouter } from 'next/router';
import React from 'react';

import features from 'constant/features';

function Index() {
  const router = useRouter();

  const handleFeatureOnClick = (e, targetUrl) => {
    e.preventDefault();

    return router.push(targetUrl);
  };

  return (
    <div className="landing-page-wrapper">
      <h2 className="landing-page-wrapper__title"> All-In-One Application</h2>
      <div className="landing-page-wrapper__creator">
        {' '}
        Created by:
        <br />
        <b> Raymond Haryanto </b>
      </div>
      <div className="landing-page-wrapper__feature-wrapper">
        {
          features.map((item) => (
            <Button
              key={item.key}
              onClick={(e) => handleFeatureOnClick(e, item.targetUrl)}
              shape="round"
              size="large"
              type="default"
            >
              {item.text}
            </Button>
          ))
        }
      </div>
    </div>
  );
}

export default Index;
