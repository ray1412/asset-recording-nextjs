import _ from 'lodash';
import Head from 'next/head';
import React, { useCallback } from 'react';
import { AnimatedOnScroll } from 'react-animated-css-onscroll';
import { VerticalTimeline, VerticalTimelineElement } from 'react-vertical-timeline-component';
import 'react-vertical-timeline-component/style.min.css';

import {
  LaptopOutlined, MailFilled, LinkedinFilled, GithubFilled, InstagramFilled, SecurityScanFilled,
} from '@ant-design/icons';

import { ADVANCED_SKILLS, INTERMEDIATE_SKILLS } from 'src/constant/skills';

const verticalTimelineIconStyle = {
  background: 'rgb(33, 150, 243)',
  color: '#fff',
  display: 'flex',
  justifyContent: 'center',
};

const PortfolioPage = () => {
  const renderAdvancedSkills = useCallback(() => _.map(ADVANCED_SKILLS, (item, idx) => {
    if (ADVANCED_SKILLS.length - 1 === idx) {
      return (
        <span key={`${item}-${idx}`}>
          and
          {' '}
          <b>{item}</b>
          .
        </span>
      );
    }

    return (
      <span key={`${item}-${idx}`}>
        <b>{item}</b>
        ,
        {' '}
      </span>
    );
  }), []);

  const renderIntermediateSkills = useCallback(() => _.map(INTERMEDIATE_SKILLS, (item, idx) => {
    if (INTERMEDIATE_SKILLS.length - 1 === idx) {
      return (
        <span key={`${item}-${idx}`}>
          and
          {' '}
          <b>{item}</b>
          .
        </span>
      );
    }

    return (
      <span key={`${item}-${idx}`}>
        <b>{item}</b>
        ,
        {' '}
      </span>
    );
  }), []);

  return (
    <>
      <Head>
        <title>Raymond Haryanto - Portfolio</title>
        <meta content="JavaScript Enthusiast, a specialist in Frontend Development" name="description" />
        <meta content="portfolio, javascript, developer, ui, ux, engineer, software, web" name="keyword" />
        <meta content="Raymond Haryanto | Front End Engineer" property="og:title" />
        <meta content="JavaScript Enthusiast, a specialist in Frontend Development" property="og:description" />
        <meta content="website" property="og:type" />
        <link
          as="style"
          href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css"
          rel="preload"
        />
        <link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css" rel="stylesheet" />
      </Head>
      <div className="portfolio-wrapper">
        <AnimatedOnScroll animationIn="fadeIn" animationOut="fadeOut">
          <section className="portfolio-main-card">
            <div className="portfolio-main-card__information-wrapper">
              <div className="portfolio-main-card__information-wrapper__content">
                <div className="portfolio-main-card__information-wrapper__content__heading">Raymond Haryanto</div>
                <div className="portfolio-main-card__information-wrapper__content__sub-heading">Software Engineer &amp; UI/UX Enthusiast</div>
                <div className="portfolio-main-card__information-wrapper__content__social-media">
                  <div className="portfolio-main-card__information-wrapper__content__social-media__item">
                    <MailFilled />
                  &nbsp;
                    <a href="mailto:raymondharyanto.rh@gmail.com">raymondharyanto.rh@gmail.com</a>
                  </div>
                  <div className="portfolio-main-card__information-wrapper__content__social-media__item">
                    <LinkedinFilled />
                  &nbsp;
                    <a href="https://www.linkedin.com/in/raymond-haryanto-685b4755/" rel="noreferrer" target="_blank">Raymond Haryanto</a>
                  </div>
                  <div className="portfolio-main-card__information-wrapper__content__social-media__item">
                    <GithubFilled />
                  &nbsp;
                    <a href="https://github.com/ray1412" rel="noreferrer" target="_blank">ray1412</a>
                  </div>
                  <div className="portfolio-main-card__information-wrapper__content__social-media__item">
                    <InstagramFilled />
                  &nbsp;
                    <a href="https://www.instagram.com/ray_haryanto/" rel="noreferrer" target="_blank">ray_haryanto</a>
                  </div>
                  <div className="portfolio-main-card__information-wrapper__content__social-media__item">
                    <SecurityScanFilled />
                  &nbsp;
                    <span>Jakarta, Indonesia</span>
                  </div>
                </div>
              </div>
            </div>
            <div className="portfolio-main-card__photo-wrapper">
              <div className="portfolio-main-card__photo-wrapper__slant" />
              <div className="portfolio-main-card__photo-wrapper__image" />
            </div>
          </section>
        </AnimatedOnScroll>
        <AnimatedOnScroll animationIn="fadeIn" animationOut="fadeOut">
          <section className="portfolio-sub-card">
            <div className="portfolio-sub-card__content">
              <p>
                Hello! I’m Raymond Haryanto. Web Developer Enthusiast with over 4 years of experience specializing in front-end development.
                Accustomed to Agile Software development and experienced in Software Development Life Cycle.
                Strong background in management and leadership.
              </p>
              <p>
                Having an in-depth knowledge including advanced
                {' '}
                {renderAdvancedSkills()}
              </p>
              <p>
                Quite understand towards
                {' '}
                {renderIntermediateSkills()}
              </p>
            </div>
            <div className="portfolio-sub-card__btn-group">
              <a className="portfolio-sub-card__btn-group__btn" href="/docs/CV_Raymond-Haryanto-2021.pdf">Download CV</a>
            </div>
          </section>
          {/* <button> check work experience </button> */}
        </AnimatedOnScroll>
        <AnimatedOnScroll animationIn="fadeInRight">
          <section className="working-experience-wrapper">
            <h2> Working Experience</h2>
            <VerticalTimeline animate={false} layout="2-columns">
              <VerticalTimelineElement
                  className="vertical-timeline-element--work"
                  date="Oct 2021 - Present"
                  icon={<LaptopOutlined />}
                  iconStyle={verticalTimelineIconStyle}
              >
                <h3 className="vertical-timeline-element-title">Lead Software Engineer</h3>
                <h4 className="vertical-timeline-element-subtitle">Indodana, PT Artha Dana Teknologi, Jakarta</h4>
                <ul>
                  <li>Helping the team in doing integration with various merchants/platforms such as Xendit, Alfamart, etc.</li>
                  <li>Helping subordinates in facing their day-to-day task and also guiding them to learn to prioritize tasks.</li>
                  <li>Conducting 1-on-1 on regular basis to find out subordinate&apos;s obstacles and gathering feedback for the team.</li>
                  <li>Contributing in system design review</li>
                  <li>Contributing in OKR planning and ensuring the team commit with it for the whole quarter </li>
                  <li>Keeping the codebase clean (THE BOY SCOUTS HAVE A RULE: “Always leave the campground cleaner than you found it.”)</li>
                </ul>
              </VerticalTimelineElement>
              <VerticalTimelineElement
                className="vertical-timeline-element--work"
                date="Mar 2020 - Oct 2021"
                icon={<LaptopOutlined />}
                iconStyle={verticalTimelineIconStyle}
              >
                <h3 className="vertical-timeline-element-title">Senior Frontend Engineer</h3>
                <h4 className="vertical-timeline-element-subtitle">Indodana, PT Artha Dana Teknologi, Jakarta</h4>
                <ul>
                  <li>Initiating setup and developing Dynamic Registration Form and Dashboard for our Whitelabel partners</li>
                  <li>Contributing to setup OpenResty & Kubernetes Configuration</li>
                  <li>Interviewing Candidate</li>
                  <li>Reviewing Code</li>
                  <li>Enhancing deployment script (Shippable) to support mono repo projects</li>
                  <li>Helping team to move from Shippable to K8s (Kubernetes)</li>
                  <li>Proposing a &ldquo;bridge&rdquo; between Webview with Android & iOS SDK</li>
                  <li>Adding and setup Sentry for logging front-end error</li>
                  <li>Helping the Engineering Manager with the technical decisions</li>
                  <li>Helping the Product Manager to breakdown a task</li>
                  <li>Collaborating with QA and Product Team to decide task/bug priority</li>
                  <li>Updating Whitelabel partners Client-Side Rendering Website to Isomorphic Website</li>
                  <li>Advising migration of Web payment page (from CSR Architecture to Isomorphic Architecture)</li>
                  <li>Securing JavaScript Source Maps from public access</li>
                </ul>
              </VerticalTimelineElement>
              <VerticalTimelineElement
                className="vertical-timeline-element--work"
                date="Feb 2019 - Mar 2020"
                icon={<LaptopOutlined />}
                iconStyle={verticalTimelineIconStyle}
              >
                <h3 className="vertical-timeline-element-title">Frontend Engineer</h3>
                <h4 className="vertical-timeline-element-subtitle">Indodana, PT Artha Dana Teknologi, Jakarta</h4>
                <ul>
                  <li>Initiating, developing, and maintaining the payment page</li>
                  <li>Initiating and developing a dashboard for our merchant</li>
                  <li>Maintaining and enhancing the internal portal</li>
                  <li>Creating own Server-side Rendered Website (not based on a framework like NextJS/NuxtJS)</li>
                  <li>Interviewing Candidate</li>
                  <li>Reviewing Code</li>
                  <li>Updating Webpack from v3 to v4, including creating chunk files, preload, prefetch</li>
                  <li>Creating a proper structure for Webpack config (there are base, dev, prod config)</li>
                  <li>Updating Project to use react-loadable</li>
                  <li>Updating Several Projects from React 16.3.0 to 16.9.0 to support React hooks</li>
                  <li>Collaborating with the marketing and SEO team to set Google Tag Manager and Meta tag</li>
                  <li>Contributing to backend development (mostly in API Layer, developed using ExpressJS)</li>
                </ul>
              </VerticalTimelineElement>
              <VerticalTimelineElement
                className="vertical-timeline-element--work"
                date="Oct 2017 - Jan 2019"
                icon={<LaptopOutlined />}
                iconStyle={verticalTimelineIconStyle}
              >
                <h3 className="vertical-timeline-element-title">Frontend Engineer</h3>
                <h4 className="vertical-timeline-element-subtitle">PT Aprisma Wirecard, Jakarta</h4>
                {/* eslint-disable-next-line max-len */}
                <p>Mainly I combine the business idea and application design into a working application that fulfils the business requirement with an excellent user experience. Four projects have been handled by me and teams, there are:</p>
                <ul>
                  <li>Mayora Mobile Banking (Indonesia)</li>
                  <li>CIMB RTB Mobile Banking (Malaysia, Thailand, Khamboja, Singapore, & Indonesia)</li>
                  <li>Branch Delivery System (A modern queuing system)</li>
                  {/* eslint-disable-next-line max-len */}
                  <li>UI Configurator (a web-based application that will support the bank&apos;s back-office, basically user just need to config what kind of fields, table, or anything to be displayed and filled by the bank&apos;s officer in order to fulfill a certain task/menu).</li>
                </ul>
              </VerticalTimelineElement>
              <VerticalTimelineElement
                className="vertical-timeline-element--work"
                date="Apr 2016 - May 2017"
                icon={<LaptopOutlined />}
                iconStyle={verticalTimelineIconStyle}
              >
                <h3 className="vertical-timeline-element-title">Junior Programmer</h3>
                <h4 className="vertical-timeline-element-subtitle">Aqua Terra Ventus Production, Jakarta</h4>
                <ul>
                  <li>Developing responsive company profile and content management system based on PHP (CodeIgniter).</li>
                  <li>Developing several Hybrid Applications using Cordova Phonegap (encapsulate HTML + CSS + JS through SDK).</li>
                </ul>
              </VerticalTimelineElement>
            </VerticalTimeline>
          </section>
        </AnimatedOnScroll>
      </div>
    </>
  );
};

PortfolioPage.propTypes = {

};

export default PortfolioPage;
