import {
  Form, Button, Select, InputNumber, Spin, message, Input,
} from 'antd';
import _ from 'lodash';
import { useRouter } from 'next/router';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import DashboardLayout from 'src/common/pages/dashboard-wrapper';
import { INVESTMENT_TYPE_ENUM, SAVING_TYPE } from 'src/constant/asset';
import {
  startCreateAsset,
  successCreateAsset,
  failCreateAsset,
} from 'src/redux/actions/asset-management/create';
import {
  getCreateAssetIsFetching,
  getCreateAssetSuccess,
  getCreateAssetError,
} from 'src/redux/reducers/selectors';
import { addAsset } from 'src/utils/firebase';

const { Option } = Select;

const CreatePage = () => {
  const router = useRouter();
  const dispatch = useDispatch();
  const isUpdatingData = useSelector(getCreateAssetIsFetching);
  const successMessage = useSelector(getCreateAssetSuccess);
  const errorMessage = useSelector(getCreateAssetError);

  const validateMessages = {
    // validation rules from ant design
    /* eslint-disable no-template-curly-in-string */
    required: '${label} is required!',
    types: {
      email: '${label} is not validate email!',
      number: '${label} is not a validate number!',
    },
    number: {
      range: '${label} must be between ${min} and ${max}',
    },
    /* eslint-enable no-template-curly-in-string */
  };

  const onCreateAsset = async (values) => {
    const {
      currentAmount, investmentReturnPerYear, investmentType, name, targetAmount, description,
    } = values;

    dispatch(startCreateAsset());

    const assetKeyId = await addAsset({
      currentAmount,
      investmentReturnPerYear,
      investmentType,
      name,
      targetAmount,
      description,
    });

    if (_.isNil(assetKeyId)) {
      return dispatch(failCreateAsset('Fail to create new asset'));
    }
    return dispatch(successCreateAsset({
      success: `${assetKeyId} has been successfully added`,
    }));
  };

  const renderSuccessMessage = () => {
    message.success(successMessage);
  };

  const renderErrorMessage = () => {
    message.error(errorMessage);
  };

  useEffect(() => {
    if (!_.isNil(successMessage)) {
      renderSuccessMessage();
    }

    if (!_.isNil(errorMessage)) {
      renderErrorMessage();
    }

    if (!_.isNil(successMessage) || !_.isNil(errorMessage)) {
      router.back();
    }
  }, [successMessage, errorMessage]);

  return (
    <DashboardLayout>
      <Spin spinning={isUpdatingData}>
        <h3 className="vertical-horizontal-center"> Form </h3>
        <br />
        <Form
          id="am-create-form"
          labelCol={{
            span: 8,
          }}
          name="add-asset-form"
          onFinish={onCreateAsset}
          validateMessages={validateMessages}
          wrapperCol={{
            span: 14,
          }}
        >
          <Form.Item label="Tujuan Investasi" required>
            <Form.Item
              name="name"
              noStyle
              rules={[{ required: true, message: 'This field is required' }]}
            >
              <Select
                allowClear
                placeholder="Pilih Tujuan Investasi"
              >
                {
                Object.keys(SAVING_TYPE).map((item) => (
                  <Option key={SAVING_TYPE[item].value} value={SAVING_TYPE[item].value}>
                    {SAVING_TYPE[item].text}
                  </Option>
                ))
              }
              </Select>
            </Form.Item>
          </Form.Item>
          <Form.Item label="Tipe Investasi" required>
            <Form.Item
              name="investmentType"
              noStyle
              rules={[{ required: true, message: 'This field is required' }]}
            >
              <Select
                allowClear
                placeholder="Pilih Tipe Investasi"
              >
                {
                Object.keys(INVESTMENT_TYPE_ENUM).map((item) => (
                  <Option key={INVESTMENT_TYPE_ENUM[item].value} value={INVESTMENT_TYPE_ENUM[item].value}>
                    {INVESTMENT_TYPE_ENUM[item].text}
                  </Option>
                ))
              }
              </Select>
            </Form.Item>
          </Form.Item>
          <Form.Item label="Target Jumlah Investasi" required>
            <Form.Item
              name="targetAmount"
              noStyle
              rules={[{ required: true, message: 'This field is required' }]}
            >
              <InputNumber
                formatter={(value) => `Rp ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                min={0}
                parser={(value) => value.replace(/Rp\s?|(,*)/g, '')}
              />
            </Form.Item>
          </Form.Item>
          <Form.Item label="Nilai Asset (sekarang)" required>
            <Form.Item
              name="currentAmount"
              noStyle
              rules={[{ required: true, message: 'This field is required' }]}
            >
              <InputNumber
                formatter={(value) => `Rp ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                min={0}
                parser={(value) => value.replace(/Rp\s?|(,*)/g, '')}
              />
            </Form.Item>
          </Form.Item>
          <Form.Item label="Return Investasi (1 tahun)" required>
            <Form.Item
              name="investmentReturnPerYear"
              noStyle
              rules={[{ required: true, message: 'This field is required' }]}
            >
              <InputNumber
                formatter={(value) => `${value}%`}
                max={100}
                min={0}
                parser={(value) => value.replace('%', '')}
                placeholder="Percentage"
                step={0.1}
              />
            </Form.Item>
          </Form.Item>
          <Form.Item label="Deskripsi" name="description">
            <Input.TextArea />
          </Form.Item>
          <Form.Item
            colon={false}
           // push 8 --> labelCol 8
            wrapperCol={{
              span: 10, pull: 4, push: 8,
            }}
          >
            <Button htmlType="submit" type="primary">
              Submit
            </Button>
          </Form.Item>
        </Form>
      </Spin>
    </DashboardLayout>
  );
};

CreatePage.propTypes = {

};

export default CreatePage;
