import { DoubleLeftOutlined } from '@ant-design/icons';
import {
  Button, Form, InputNumber, Select, Spin, message, Input,
} from 'antd';
import _ from 'lodash';
import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { INVESTMENT_TYPE_ENUM, SAVING_TYPE } from 'constant/asset';
import DashboardLayout from 'src/common/pages/dashboard-wrapper';
import {
  startUpdateAsset,
  successUpdateAsset,
  failUpdateAsset,
  doResetEditAssetManagement,
} from 'src/redux/actions/asset-management/edit';
import {
  getEditAssetData,
  getEditAssetIsFetching,
  getEditAssetSuccess,
  getEditAssetError,
} from 'src/redux/reducers/selectors';
import { updateAsset, getParticularAsset } from 'utils/firebase';


const { Option } = Select;

const EditPage = () => {
  const router = useRouter();
  const dispatch = useDispatch();
  const currentSelectedAsset = useSelector(getEditAssetData);
  const isUpdatingData = useSelector(getEditAssetIsFetching);
  const successMessage = useSelector(getEditAssetSuccess);
  const errorMessage = useSelector(getEditAssetError);
  const [formInitialValue, setFormInitialValue] = useState({});
  const [isFetchingData, setIsFetchingData] = useState(false);
  const validateMessages = {
    // validation rules from ant design
    /* eslint-disable no-template-curly-in-string */
    required: '${label} is required!',
    types: {
      email: '${label} is not validate email!',
      number: '${label} is not a validate number!',
    },
    number: {
      range: '${label} must be between ${min} and ${max}',
    },
    /* eslint-enable no-template-curly-in-string */
  };

  useEffect(() => {
    if (_.isEmpty(currentSelectedAsset)) {
      setIsFetchingData(true);
      const { pathname } = window.location;
      const id = _.chain(pathname)
        .split('/')
        .last()
        .value();

      getParticularAsset(id)
        .then((item) => {
          setFormInitialValue({
            assetId: id,
            ...item,
          });
        })
        .catch((e) => {
          console.error(e);
        })
        .finally(() => {
          setIsFetchingData(false);
        });
    } else {
      setFormInitialValue(currentSelectedAsset);
    }

    return () => {
      dispatch(doResetEditAssetManagement());
    };
  }, []);

  const renderSuccessMessage = () => {
    message.success(successMessage);
  };

  const renderErrorMessage = () => {
    message.error(errorMessage);
  };

  const onUpdateAsset = (values) => {
    const {
      currentAmount,
      investmentReturnPerYear,
      investmentType,
      name,
      targetAmount,
      description,
    } = values;

    const {
      assetId,
      createdAt,
      activityLog,
    } = formInitialValue;

    dispatch(startUpdateAsset());

    return updateAsset({
      assetId,
      currentAmount,
      investmentReturnPerYear,
      investmentType,
      name,
      targetAmount,
      description,
      createdAt,
      activityLog,
      callBack: (error, id) => {
        if (error) {
          console.error(`Fail to update ${id}`);
          return dispatch(failUpdateAsset(error));
        }

        console.log(`${id} has been successfully updated`);
        router.back();
        return dispatch(successUpdateAsset(`${assetId} has been successfully updated`));
      },
    });
  };

  useEffect(() => {
    if (!_.isNil(successMessage)) {
      renderSuccessMessage();
    }

    if (!_.isNil(errorMessage)) {
      renderErrorMessage();
    }
  }, [successMessage, errorMessage]);

  /**
   * TODO make button and header in one line
   */
  return (
    <DashboardLayout>
      <div style={{ display: 'flex' }}>
        <Button
          icon={<DoubleLeftOutlined />}
          onClick={() => router.back()}
          shape="circle"
          size="small"
          type="default"
        />
        &nbsp;
        <h3 className="vertical-horizontal-center"> Edit Asset </h3>
      </div>
      <br />
      <div className="am-edit-wrapper">
        <Spin spinning={isUpdatingData || isFetchingData}>
          {
            !_.isEmpty(formInitialValue)
            && (
              <Form
                id="am-edit-form"
                initialValues={formInitialValue}
                labelCol={{
                  span: 8,
                }}
                name="add-asset-form"
                onFinish={onUpdateAsset}
                validateMessages={validateMessages}
                wrapperCol={{
                  span: 14,
                }}
              >
                <Form.Item label="Tujuan Investasi">
                  <Form.Item
                    name="name"
                    noStyle
                    rules={[{ required: true, message: 'This field is required' }]}
                    // initialValue={formInitialValue.name}
                  >
                    <Select
                      allowClear
                      placeholder="Pilih Tujuan Investasi"
                    >
                      {
                        Object.keys(SAVING_TYPE).map((item) => (
                          <Option key={SAVING_TYPE[item].value} value={SAVING_TYPE[item].value}>
                            {SAVING_TYPE[item].text}
                          </Option>
                        ))
                      }
                    </Select>
                  </Form.Item>
                </Form.Item>
                <Form.Item label="Tipe Investasi">
                  <Form.Item
                    name="investmentType"
                    noStyle
                    rules={[{ required: true, message: 'This field is required' }]}
                  >
                    <Select
                      allowClear
                      placeholder="Pilih Tipe Investasi"
                    >
                      {
                        Object.keys(INVESTMENT_TYPE_ENUM).map((item) => (
                          <Option key={INVESTMENT_TYPE_ENUM[item].value} value={INVESTMENT_TYPE_ENUM[item].value}>
                            {INVESTMENT_TYPE_ENUM[item].text}
                          </Option>
                        ))
                      }
                    </Select>
                  </Form.Item>
                </Form.Item>
                <Form.Item label="Target Jumlah Investasi">
                  <Form.Item
                    name="targetAmount"
                    noStyle
                    rules={[{ required: true, message: 'This field is required' }]}
                  >
                    <InputNumber
                      formatter={(value) => `Rp ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                      min={0}
                      parser={(value) => value.replace(/Rp\s?|(,*)/g, '')}
                    />
                  </Form.Item>
                </Form.Item>
                <Form.Item label="Nilai Asset (sekarang)">
                  <Form.Item
                    name="currentAmount"
                    noStyle
                    rules={[{ required: true, message: 'This field is required' }]}
                  >
                    <InputNumber
                      formatter={(value) => `Rp ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                      min={0}
                      parser={(value) => value.replace(/Rp\s?|(,*)/g, '')}
                    />
                  </Form.Item>
                </Form.Item>
                <Form.Item label="Return Investasi (1 tahun)">
                  <Form.Item
                    name="investmentReturnPerYear"
                    noStyle
                    rules={[{ required: true, message: 'This field is required' }]}
                  >
                    <InputNumber
                      formatter={(value) => `${value}%`}
                      max={100}
                      min={0}
                      parser={(value) => value.replace('%', '')}
                      placeholder="Percentage"
                      step={0.1}
                    />
                  </Form.Item>
                </Form.Item>
                <Form.Item label="Deskripsi" name="description">
                  <Input.TextArea />
                </Form.Item>
                <Form.Item
                  colon={false}
                  // push 8 --> labelCol 8
                  wrapperCol={{
                    span: 10, pull: 4, push: 8,
                  }}
                >
                  <Button htmlType="submit" type="primary">
                    Submit
                  </Button>
                </Form.Item>
              </Form>
            )
          }
        </Spin>
      </div>
    </DashboardLayout>
  );
};

EditPage.propTypes = {

};

export default EditPage;
