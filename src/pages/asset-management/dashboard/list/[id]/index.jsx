import { EditOutlined, DoubleLeftOutlined } from '@ant-design/icons';
import { Descriptions, Button, Spin } from 'antd';
import dayjs from 'dayjs';
import _ from 'lodash';
import dynamic from 'next/dynamic';
import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import DashboardLayout from 'src/common/pages/dashboard-wrapper';
import { INVESTMENT_TABLE_HEADER_MAPPING, INVESTMENT_TYPE_ENUM, SAVING_TYPE } from 'src/constant/asset';
import { doResetDetailManagement } from 'src/redux/actions/asset-management/detail';
import { doEditAsset } from 'src/redux/actions/asset-management/edit';
import { getDetailAssetData } from 'src/redux/reducers/selectors';
import { routes } from 'src/routes';
import currencyUtils from 'utils/currency';
import { getParticularAsset } from 'utils/firebase';

const Chart = dynamic(() => import('react-apexcharts'), { ssr: false });

const DetailPage = () => {
  const dispatch = useDispatch();
  const router = useRouter();
  const currentSelectedAsset = useSelector(getDetailAssetData);
  const [formInitialValue, setFormInitialValue] = useState({});
  const [isFetchingData, setIsFetchingData] = useState(false);
  const [isReadyToRenderChart, setIsReadyToRenderChart] = useState(false);
  const [chartData, setChartData] = useState({
    series: [{
      name: 'Asset',
      data: [],
    }],
    options: {
      chart: {
        width: '100%',
        height: 350,
        type: 'line',
        id: 'activity-log-chart',
      },
      stroke: {
        curve: 'straight',
      },
      grid: {
        padding: {
          right: 30,
          left: 20,
        },
      },
      title: {
        text: 'Activity log',
        align: 'left',
      },
      labels: '',
      yaxis: {
        title: {
          text: 'Rupiah',
        },
      },
      xaxis: {
        type: 'datetime',
      },
    },
  });

  const goToEditAsset = () => {
    const { assetId } = formInitialValue;

    dispatch(doEditAsset({ ...formInitialValue }));

    return router.push({
      pathname: routes.ASSET_MANAGEMENT_DASHBOARD_EDIT_PAGE.page,
      query: { id: assetId },
    });
  };

  const renderContent = () => {
    if (_.isEmpty(formInitialValue)) {
      return null;
    }

    const listOfKeys = _.chain(INVESTMENT_TABLE_HEADER_MAPPING)
      .omit(['assetId', 'action'])
      .keys()
      .value();

    // TODO create constant for this switch case
    return _.map(listOfKeys, (item) => {
      let mutatedItem = formInitialValue[item];

      switch (item) {
        case 'name': {
          const targetedObj = _.find(SAVING_TYPE, (constant) => constant.value === formInitialValue[item]);
          mutatedItem = targetedObj.text;
          break;
        }
        case 'currentAmount': {
          mutatedItem = currencyUtils.format({ number: formInitialValue[item] });
          break;
        }
        case 'targetAmount': {
          mutatedItem = currencyUtils.format({ number: formInitialValue[item] });
          break;
        }
        case 'investmentReturnPerYear': {
          mutatedItem = `${formInitialValue[item]} %`;
          break;
        }
        case 'investmentType': {
          const targetedObj = _.find(INVESTMENT_TYPE_ENUM, (constant) => constant.value === formInitialValue[item]);
          mutatedItem = targetedObj.text;
          break;
        }
        default:
          break;
      }

      return (
        <Descriptions.Item key={item} label={INVESTMENT_TABLE_HEADER_MAPPING[item]}>
          <b>{mutatedItem}</b>
        </Descriptions.Item>
      );
    });
  };

  useEffect(() => {
    if (_.isEmpty(currentSelectedAsset)) {
      setIsFetchingData(true);
      const { pathname } = window.location;
      const id = _.chain(pathname)
        .split('/')
        .last()
        .value();

      getParticularAsset(id)
        .then((item) => {
          setFormInitialValue({
            assetId: id,
            ...item,
          });
        })
        .catch((e) => {
          console.error(e);
        })
        .finally(() => {
          setIsFetchingData(false);
        });
    } else {
      setFormInitialValue(currentSelectedAsset);
    }

    return () => {
      dispatch(doResetDetailManagement());
    };
  }, []);

  useEffect(() => {
    if (!_.isEmpty(formInitialValue)) {
      const { activityLog } = formInitialValue;
      const xSeries = _.chain(activityLog)
        .keys(activityLog)
        .map((date) => dayjs.unix(date).format())
        .value();
      const ySeries = _.map(activityLog, (data) => data.currentAmount);

      setChartData((prevState) => ({
        ...prevState,
        series: [{
          data: ySeries,
        }],
        options: {
          ...prevState.options,
          labels: xSeries,
        },
      }));

      // hackish way to prevent apexchart `width` bug
      setTimeout(() => {
        setIsReadyToRenderChart(true);
      }, 375);
    }
  }, [formInitialValue]);

  return (
    <DashboardLayout>
      <Spin spinning={isFetchingData}>
        <Descriptions
          bordered
          column={{
            xxl: 4, xl: 3, lg: 3, md: 3, sm: 2, xs: 1,
          }}
          extra={(
            <Button
              icon={<EditOutlined />}
              onClick={goToEditAsset}
              type="primary"
            >
              Edit
            </Button>
          )}
          title={(
            <div style={{ display: 'flex' }}>
              <Button
                icon={<DoubleLeftOutlined />}
                onClick={() => router.back()}
                shape="circle"
                size="small"
                type="default"
              />
              &nbsp;
              <div> Detail Asset </div>
            </div>
          )}
        >
          {
            !_.isEmpty(formInitialValue)
            && renderContent()
          }
        </Descriptions>
        <br />
        <br />
        {
          isReadyToRenderChart
          && (
            <Chart height={350} options={chartData.options} series={chartData.series} type="line" />
          )
        }
      </Spin>
    </DashboardLayout>
  );
};

DetailPage.propTypes = {

};

export default DetailPage;
