import { DeleteOutlined, EyeFilled } from '@ant-design/icons';
import { Button, Empty, Modal } from 'antd';
import _ from 'lodash';
import { useRouter } from 'next/router';
import propTypes from 'prop-types';
import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import {
  Table, Thead, Tbody, Tr, Th, Td,
} from 'react-super-responsive-table';

import DashboardLayout from 'src/common/pages/dashboard-wrapper';
import { SAVING_TYPE, INVESTMENT_TABLE_HEADER_MAPPING } from 'src/constant/asset';
import { doCheckAssetDetail } from 'src/redux/actions/asset-management/detail';
import { routes } from 'src/routes';
import currencyUtils from 'utils/currency';
import { getAllAsset, removeAsset } from 'utils/firebase';

const ListingPage = (props) => {
  const { error } = props;
  const router = useRouter();
  const dispatch = useDispatch();
  const { data } = props;

  // TODO create constant for removing those fields
  // omitting non necessary things to be displayed in list
  const tableHeader = _.omit(INVESTMENT_TABLE_HEADER_MAPPING, [
    'assetId',
    'investmentType',
    'targetAmount',
    'investmentReturnPerYear',
  ]);

  const [modalState, setModalState] = useState(false);
  const [selectedItem, setSelectedItem] = useState(null);
  const [confirmLoading, setConfirmLoading] = useState(false);
  const toggleModalState = () => setModalState((prevState) => !prevState);
  const toggleConfirmLoading = () => setConfirmLoading((prevState) => !prevState);

  const goToDetailAsset = ({ assetId, investmentData }) => {
    dispatch(doCheckAssetDetail({
      ...investmentData,
      assetId,
    }));

    return router.push({
      pathname: routes.ASSET_MANAGEMENT_DASHBOARD_DETAIL_PAGE.page,
      query: { id: assetId },
    });
  };

  const deleteAsset = (assetId) => removeAsset(assetId)
    .then(() => {
      toggleConfirmLoading();
      router.reload();
    })
    .catch((e) => {
      console.error(`Remove failed: ${e.message}`);
    });

  const backToLogin = () => router.push(routes.ASSET_MANAGEMENT_LOGIN_PAGE.page);

  const constructedDataTable = _.reduce(data, (acc, item, key) => {
    const convertedCurrentAmount = currencyUtils.format({ number: item.currentAmount });

    return [
      ...acc,
      {
        ...item,
        primaryKey: key,
        currentAmount: convertedCurrentAmount,
        action: (
          <>
            <Button
              icon={<EyeFilled />}
              onClick={() => goToDetailAsset({
                assetId: key,
                investmentData: item,
              })}
              shape="round"
              size="medium"
              type="primary"
            >
              Detail
            </Button>
            &nbsp;
            <Button
              danger
              icon={<DeleteOutlined />}
              onClick={() => {
                setSelectedItem(key);
                return toggleModalState();
              }}
              shape="round"
              size="medium"
              type="primary"
            >
              Delete
            </Button>
          </>
        ),
      },
    ];
  }, []);
  const calculatedCurrentAmount = currencyUtils.format({
    number: _.chain(data)
      .values()
      .sumBy('currentAmount')
      .value(),
  });

  return (
    <DashboardLayout>
      <h2 style={{ textAlign: 'center' }}> Asset List </h2>
      {
        _.isEmpty(constructedDataTable)
          ? (
            <Empty
              description={(
                <span>
                  {error}
                </span>
            )}
            >
              <Button onClick={backToLogin} type="primary">Back to Login</Button>
            </Empty>
          )
          : (
            <>
              <Table>
                <Thead>
                  <Tr>
                    {Object.values(tableHeader).map((item) => (
                      <Th key={item}>
                        {item}
                      </Th>
                    ))}
                  </Tr>
                </Thead>
                <Tbody>
                  {
                    constructedDataTable.map((item) => {
                      const valueSavingType = _.findKey(SAVING_TYPE, (yoo) => yoo.value === item.name);

                      return (
                        <Tr key={item.primaryKey}>
                          <Td><b>{SAVING_TYPE[valueSavingType].text}</b></Td>
                          <Td>{_.get(item, 'currentAmount', '-')}</Td>
                          <Td>{_.get(item, 'description', '-')}</Td>
                          <Td>{item.action}</Td>
                        </Tr>
                      );
                    })
                  }
                </Tbody>
              </Table>
              <br />
              <h3>
                Total Asset:
                {' '}
                {calculatedCurrentAmount}
              </h3>
            </>
          )
      }
      <Modal
        confirmLoading={confirmLoading}
        onCancel={toggleModalState}
        onOk={() => {
          toggleConfirmLoading();
          deleteAsset(selectedItem);
        }}
        title="Apakah Anda Yakin?"
        visible={modalState}
      >
        <p>Kamu akan menghapus data ini</p>
      </Modal>
    </DashboardLayout>
  );
};

ListingPage.propTypes = {
  data: propTypes.instanceOf(Object).isRequired,
  error: propTypes.string,
};

ListingPage.defaultProps = {
  error: '',
};

export async function getServerSideProps({ req }) {
  return getAllAsset({ req }).then((data) => ({
    props: {
      data,
    },
  })).catch((e) => ({
    props: {
      error: e.toString(),
      data: {},
    },
  }));
}

export default ListingPage;
