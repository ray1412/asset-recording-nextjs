import { MailFilled, LockOutlined } from '@ant-design/icons';
import {
  Form, Input, Button, Alert,
} from 'antd';
import _ from 'lodash';
import Router from 'next/router';
import React, { useState } from 'react';

import { routes } from 'src/routes';
import { doFirebaseLogin } from 'src/utils/firebase';
import {loginWithGoogleAuthProvider} from 'src/utils/firebase'
import storageUtils from 'src/utils/storage';

const AssetManagementLogin = () => {
  const [errorMessage, setErrorMessage] = useState('');
  const [isVerifyingUser, setIsUserVerifying] = useState(false);

  const doLogin = ({ email, password }) => {
    setIsUserVerifying((prevState) => !prevState);

    doFirebaseLogin({
      email,
      password,
    })
      .then((userCredential) => {
        setErrorMessage('');
        const { user } = userCredential;
        const { email: userEmail, lastLoginAt, uid } = user.toJSON();

        storageUtils.set(
          storageUtils.userCredentialKey,
          JSON.stringify({
            email: userEmail,
            lastLoginAt,
            uid,
          }),
        );


        return user.getIdToken()
          .then(() => Router.push(routes.ASSET_MANAGEMENT_DASHBOARD_LIST_PAGE.page));
      })
      .catch((err) => {
        setErrorMessage(err.message);
      })
      .finally(() => {
        setIsUserVerifying((prevState) => !prevState);
      });
  };

  return (
    <div className="asset-management-login-wrapper">
      <h2> Asset Management Login </h2>
      <br />
      <Form
        className="asset-management-login-form"
        initialValues={{
          remember: true,
        }}
        name="basic"
        onFinish={doLogin}
        size="large"
      >
        <Form.Item
          className="asset-management-login-form__item"
          name="email"
          rules={[
            {
              required: true,
              message: 'Please input your email!',
            },
          ]}
          type="email"
        >
          <Input
            autoComplete="email"
            placeholder="Email"
            prefix={<MailFilled className="site-form-item-icon" />}
          />
        </Form.Item>
        <Form.Item
          className="asset-management-login-form__item"
          name="password"
          rules={[
            {
              required: true,
              message: 'Please input your password!',
            },
          ]}
        >
          <Input
            autoComplete="current-password"
            placeholder="Password"
            prefix={<LockOutlined className="site-form-item-icon" />}
            type="password"
          />
        </Form.Item>
        {
          !_.isEmpty(errorMessage)
          && <Alert message={errorMessage} showIcon type="error" />
        }
        <Form.Item className="asset-management-login-form__button-wrapper">
          <Button
            className="asset-management-login-form__button-wrapper__button"
            htmlType="submit"
            loading={isVerifyingUser}
            type="primary"
          >
            Submit
          </Button>
        </Form.Item>
      </Form>
      <Button
        className="asset-management-login-form__button-wrapper__button"
        onClick={loginWithGoogleAuthProvider}
        type="primary"
        // loading={isVerifyingUser}
      >
        Login with own BE
      </Button>
    </div>
  );
};

export async function getServerSideProps({ req, res }) {
  /**
   * TODO kalo udah pernah login redirect --> baca cookie / hit ke firebase dlu
   */

  return { props: {} };
}

export default AssetManagementLogin;
