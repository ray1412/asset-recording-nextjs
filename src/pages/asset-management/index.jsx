import { Spin } from 'antd';
import _ from 'lodash';
import { useRouter } from 'next/router';
import React, { useEffect } from 'react';

import { routes } from 'src/routes';
import { doVerifyUser } from 'utils/firebase';
import storageUtils from 'utils/storage';

const AssetManagement = () => {
  const router = useRouter();

  useEffect(() => {
    doVerifyUser((user) => {
      const isLoggedIn = !_.isEmpty(user);

      
      /**
       * TODO
       * Get user access token (JWT)
       * const token = await user.getIdToken()
       */
      if (isLoggedIn) {
        const { email, uid } = user;

        storageUtils.set(
          storageUtils.userCredentialKey,
          JSON.stringify({
            email,
            uid,
          }),
        );
        router.push(routes.ASSET_MANAGEMENT_DASHBOARD_LIST_PAGE.page);
      } else {
        router.push(routes.ASSET_MANAGEMENT_LOGIN_PAGE.page);
      }
    });
  }, []);

  return (
    <div className="asset-management-landing-wrapper vertical-horizontal-center">
      <Spin size="large" />
    </div>
  );
};

AssetManagement.propTypes = {

};

export default AssetManagement;
