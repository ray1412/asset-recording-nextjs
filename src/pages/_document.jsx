import Document, {
  Html, Head, Main, NextScript,
} from 'next/document';
import React from 'react';

class CustomDocument extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx);
    return { ...initialProps }
  }

  render() {
    return (
      <Html>
        <meta charSet="utf-8" />
        <meta
          content="width=device-width, initial-scale=1,shrink-to-fit=no"
          name="viewport"
        />
        {/* The core Firebase JS SDK is always required and must be listed first */}
        <script src="https://www.gstatic.com/firebasejs/7.14.6/firebase-app.js" />
        <script src="https://www.gstatic.com/firebasejs/7.14.6/firebase-analytics.js" />
        <script src="https://www.gstatic.com/firebasejs/7.14.6/firebase-auth.js" />
        <script src="https://www.gstatic.com/firebasejs/7.14.6/firebase-database.js" />
        <link href="https://fonts.googleapis.com/css2?family=Raleway:ital,wght@0,300;0,500;0,900;1,300;1,500;1,900&display=swap" rel="stylesheet" />
        <link href="https://unpkg.com/nprogress@0.2.0/nprogress.css" rel="stylesheet" />
        <link href="/favicon.ico" rel="icon" type="image/x-icon" />
        <link href="/favicon-16x16.png" rel="icon" sizes="16x16" type="image/x-icon" />
        <link href="/favicon-32x32.png" rel="icon" sizes="32x32" type="image/x-icon" />
        <link href="/android-chrome-192x192.png" rel="icon" sizes="192x192" type="image/x-icon" />
        <link href="/android-chrome-512x512.png" rel="icon" sizes="512x512" type="image/x-icon" />
        <link href="/apple-touch-icon.png" rel="apple-touch-icon-precomposed" type="image/x-icon" />
        <Head>
          <style>
            {
              `#__next {
                  height: 100%
                }
              `
            }
          </style>
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}

// https://nextjs.org/docs/advanced-features/custom-document
// The ctx object is equivalent to the one received in getInitialProps, with one addition:
// renderPage: Function - a callback that runs the actual React rendering logic (synchronously).
// It's useful to decorate this function in ord
export default CustomDocument;
