import React, { useState } from 'react';
import Link from 'next/link';
import { Layout, Menu } from 'antd';
import { LaptopOutlined, EditOutlined } from '@ant-design/icons';

import { routes } from 'src/routes';

const DashboardSidebar = () => {
  const [isSidebarOpen, setIsSidebarOpen] = useState(false);
  const {
    Sider,
  } = Layout;

  const listMenu = [
    {
      key: 'list',
      displayText: 'Asset List',
      url: routes.ASSET_MANAGEMENT_DASHBOARD_LIST_PAGE.page,
      icon: <LaptopOutlined />,
    },
    {
      key: 'create',
      displayText: 'Add Asset',
      url: routes.ASSET_MANAGEMENT_DASHBOARD_CREATE_PAGE.page,
      icon: <EditOutlined />,
    },
  ];

  return (
    <Sider
      collapsible
      collapsed={isSidebarOpen}
      breakpoint="lg"
      onCollapse={(collapsed) => {
        setIsSidebarOpen(collapsed);
      }}
    >
      <div className="logo">
        <img src="https://raw.githubusercontent.com/ray1412/foreign-exchange-currency/master/ray1412-banner.png" alt="ray1412-logo" />
      </div>
      <Menu
        theme="dark"
        defaultSelectedKeys={['1']}
        mode="inline"
      >
        {
          listMenu.map((menu) => (
            <Menu.Item key={menu.key} icon={menu.icon}>
              <Link href={menu.url} scroll>
                <a>{menu.displayText}</a>
              </Link>
            </Menu.Item>
          ))
         }
      </Menu>
    </Sider>
  );
};

export default DashboardSidebar;
