import React from 'react';
import PropTypes from 'prop-types';
import {
  Layout, Button, PageHeader,
} from 'antd';
import Router from 'next/router';

import { doFirebaseLogout } from 'utils/firebase';
import DashboardSidebar from 'src/common/component/dashboard-sidebar';
import storageUtils from 'utils/storage';

const AssetManagementDashboardWrapper = (props) => {
  const { children } = props;
  const {
    Content, Footer,
  } = Layout;

  const logout = () => {
    storageUtils.clear();

    doFirebaseLogout()
      .then(() => Router.replace('/asset-management/login'))
      .catch((err) => console.error(err));
  };

  return (
    <Layout className="asset-management-dashboard-wrapper">
      <DashboardSidebar />
      <Layout className="asset-management-dashboard-container">
        <PageHeader
          className="asset-management-dashboard-header"
          extra={[
            <Button key="1" onClick={logout}>Logout</Button>,
          ]}
        />
        <Content className="main-content-wrapper">
          <div className="main-content-container">
            {children}
          </div>
        </Content>
        <Footer className="asset-management-dashboard-footer">
          Asset Management ©2020 Created by Raymond Haryanto
        </Footer>
      </Layout>
    </Layout>
  );
};

AssetManagementDashboardWrapper.propTypes = {
  children: PropTypes.node.isRequired,
};

export default AssetManagementDashboardWrapper;
