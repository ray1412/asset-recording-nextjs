import actionTypes from 'src/common/redux/action-types';

const defaultState = '';

const counterReducer = (state = defaultState, action) => {
  switch (action.type) {
    case actionTypes.SET_ACTIVE_SIDEBAR_MENU: {
      return action.payload.data;
    }
    case actionTypes.RESET_ACTIVE_SIDEBAR_MENU: {
      return defaultState;
    }
    default: {
      return state;
    }
  }
};

export default counterReducer;
