import { useMemo } from 'react';
import { createStore } from 'redux';
import { fromJS, Map } from 'immutable';
import { bindMiddleware } from './utils';
import reducers from './reducers';

function initStore(initialState = Map({})) {
  return createStore(
    reducers,
    initialState,
    bindMiddleware([]),
  );
}

let cachedStore;

export const getCachedStore = () => cachedStore;

export const initializeStore = (preloadedState) => {
  let store = cachedStore ?? initStore(preloadedState);

  // After navigating to a page with an initial Redux state, merge that state
  // with the current state in the store, and create a new store
  if (preloadedState && cachedStore) {
    store = initStore(
      cachedStore.getState().merge(preloadedState),
    );

    // Reset the current store
    cachedStore = undefined;
  }

  // if in server, for SSG and SSR always create a new store
  if (typeof window === 'undefined') {
    return store;
  }

  // Create the store once in the client
  if (!cachedStore) {
    cachedStore = store;
  }

  return store;
};

export function useStore(initialState) {
  return useMemo(
    () => initializeStore(fromJS(initialState)),
    [initialState],
  );
}
