export default {
  GO_TO_DETAIL_ASSET: 'app/asset-management/check-detail-asset',
  RESET_DETAIL_ASSET: 'app/asset-management/reset-detail-asset',

  GO_TO_EDIT_ASSET: 'app/asset-management/update-edit-asset',
  RESET_EDIT_ASSET: 'app/asset-management/reset-edit-asset',
  START_UPDATE_EDITED_ASSET: 'app/asset-management/start-update-edited-asset',
  SUCCESS_UPDATE_EDITED_ASSET: 'app/asset-management/success-update-edited-asset',
  FAIL_UPDATE_EDITED_ASSET: 'app/asset-management/fail-update-edited-asset',

  START_CREATE_ASSET: 'app/asset-management/start-create-asset',
  SUCCESS_CREATE_ASSET: 'app/asset-management/success-create-asset',
  FAIL_CREATE_ASSET: 'app/asset-management/fail-create-asset',
};
