import actionTypes from 'src/redux/action-types';

export const startCreateAsset = () => ({
  type: actionTypes.START_CREATE_ASSET,
});

export const successCreateAsset = ({ data, success }) => ({
  type: actionTypes.SUCCESS_CREATE_ASSET,
  payload: {
    data,
    success,
  },
});

export const failCreateAsset = (data) => ({
  type: actionTypes.FAIL_CREATE_ASSET,
  payload: {
    data,
  },
});
