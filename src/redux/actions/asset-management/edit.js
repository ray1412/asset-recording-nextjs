import actionTypes from 'src/redux/action-types';

export const doEditAsset = (data) => ({
  type: actionTypes.GO_TO_EDIT_ASSET,
  payload: {
    data,
  },
});

export const startUpdateAsset = () => ({
  type: actionTypes.START_UPDATE_EDITED_ASSET,
});

export const successUpdateAsset = (data) => ({
  type: actionTypes.SUCCESS_UPDATE_EDITED_ASSET,
  payload: {
    data,
  },
});

export const failUpdateAsset = (data) => ({
  type: actionTypes.FAIL_UPDATE_EDITED_ASSET,
  payload: {
    data,
  },
});

export const doResetEditAssetManagement = () => ({
  type: actionTypes.RESET_EDIT_ASSET,
});
