import actionTypes from 'src/redux/action-types';

export const doCheckAssetDetail = (data) => ({
  type: actionTypes.GO_TO_DETAIL_ASSET,
  payload: {
    data,
  },
});

export const doResetDetailManagement = () => ({
  type: actionTypes.RESET_DETAIL_ASSET,
});
