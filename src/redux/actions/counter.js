import actionTypes from 'src/redux/action-types';

export const incrementCounter = () => ({
  type: actionTypes.INCREMENT_COUNTER
});

export const decrementCounter = () => ({
  type: actionTypes.DECREMENT_COUNTER
});