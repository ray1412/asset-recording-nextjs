// ================= Get Create Asset =================
export const getCreateAssetData = (state) => state.asset.create.data;
export const getCreateAssetIsFetching = (state) => state.asset.create.isFetching;
export const getCreateAssetSuccess = (state) => state.asset.create.success;
export const getCreateAssetError = (state) => state.asset.create.error;
// ==================================================


// ================= Get Edit Asset =================
export const getEditAssetData = (state) => state.asset.edit.data;
export const getEditAssetIsFetching = (state) => state.asset.edit.isFetching;
export const getEditAssetSuccess = (state) => state.asset.edit.success;
export const getEditAssetError = (state) => state.asset.edit.error;
// ==================================================

// ================= Get Detail Asset =================
export const getDetailAssetData = (state) => state.asset.detail.data;
export const getDetailAssetIsFetching = (state) => state.asset.detail.isFetching;
export const getDetailAssetSuccess = (state) => state.asset.detail.success;
export const getDetailAssetError = (state) => state.asset.detail.error;
// ==================================================
