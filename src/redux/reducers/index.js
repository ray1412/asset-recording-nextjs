import { HYDRATE } from 'next-redux-wrapper';
import { combineReducers } from 'redux';
import commonReducer from 'src/common/redux/reducer';
import createAssetManagementReducer from 'src/redux/reducers/asset-management/create';
import detailAssetManagementReducer from 'src/redux/reducers/asset-management/detail';
import editAssetManagementReducer from 'src/redux/reducers/asset-management/edit';

const combinedReducer = combineReducers({
  common: commonReducer,
  asset: combineReducers({
    create: createAssetManagementReducer,
    detail: detailAssetManagementReducer,
    edit: editAssetManagementReducer,
  }),
});

const rootReducer = (state, action) => {
  if (action.type === HYDRATE) {
    return {
      ...state, // use previous state
      ...action.payload, // apply delta from hydration
    };
  }
  return combinedReducer(state, action);
};


export default rootReducer;
