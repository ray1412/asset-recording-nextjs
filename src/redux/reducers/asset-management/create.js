import actionTypes from 'src/redux/action-types';

const defaultState = {
  data: {},
  isFetching: false,
  success: null,
  error: null,
};

const counterReducer = (state = defaultState, action) => {
  switch (action.type) {
    case actionTypes.START_CREATE_ASSET: {
      return { ...state, isFetching: true };
    }
    case actionTypes.SUCCESS_CREATE_ASSET: {
      return {
        isFetching: false,
        data: action.payload.data,
        success: action.payload.success,
        error: null,
      };
    }
    case actionTypes.FAIL_CREATE_ASSET: {
      return {
        ...state,
        isFetching: false,
        success: null,
        error: action.payload.data,
      };
    }
    case actionTypes.RESET_EDIT_ASSET: {
      return defaultState;
    }
    default: {
      return state;
    }
  }
};

export default counterReducer;
