import actionTypes from 'src/redux/action-types';

const defaultState = {
  data: {},
  isFetching: false,
  success: null,
  error: null,
};

const detailAssetReducer = (state = defaultState, action) => {
  switch (action.type) {
    case actionTypes.GO_TO_DETAIL_ASSET: {
      return { ...state, data: action.payload.data };
    }
    case actionTypes.RESET_DETAIL_ASSET: {
      return defaultState;
    }
    default: {
      return state;
    }
  }
};

export default detailAssetReducer;
