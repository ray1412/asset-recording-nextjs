import actionTypes from 'src/redux/action-types';

const defaultState = {
  data: {},
  isFetching: false,
  success: null,
  error: null,
};

const editAssetReducer = (state = defaultState, action) => {
  switch (action.type) {
    case actionTypes.GO_TO_EDIT_ASSET: {
      return { ...state, data: action.payload.data };
    }
    case actionTypes.START_UPDATE_EDITED_ASSET: {
      return {
        ...state, isFetching: true, success: null, error: null,
      };
    }
    case actionTypes.SUCCESS_UPDATE_EDITED_ASSET: {
      return {
        ...state, isFetching: false, success: action.payload.data, error: null,
      };
    }
    case actionTypes.FAIL_UPDATE_EDITED_ASSET: {
      return { ...state, isFetching: false, error: action.payload.data };
    }
    case actionTypes.RESET_EDIT_ASSET: {
      return defaultState;
    }
    default: {
      return state;
    }
  }
};

export default editAssetReducer;
