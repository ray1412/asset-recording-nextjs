import { Map, fromJS } from 'immutable';
import { applyMiddleware } from 'redux';

export const bindMiddleware = (middlewares) => {
  if (process.env.NODE_ENV !== 'production') {
    // eslint-disable-next-line global-require
    const { composeWithDevTools } = require('redux-devtools-extension');
    return composeWithDevTools(applyMiddleware(...middlewares));
  }

  return applyMiddleware(...middlewares);
};

export const getReduxProps = (store) => ({
  initialReduxState: store.getState().toJS(),
});

export function createCommonMapReducer(name) {
  const initialState = Map({
    loading: false,
    success: false,
    failure: false,
    error: null,
    data: null,
  });

  const reducer = (state = initialState, action) => {
    switch (action.type) {
      case `${name}_START`:
        return state.merge({
          loading: true,
          success: false,
          failure: false,
          error: null,
        });

      case `${name}_SUCCESS`:
        return state.merge({
          loading: false,
          success: true,
          failure: false,
          error: null,
          data: fromJS(action.payload),
        });

      case `${name}_FAILURE`:
        return state.merge({
          loading: false,
          success: false,
          failure: true,
          error: action.payload,
        });

      case `${name}_RESET`:
        return initialState;

      default:
        return state;
    }
  };

  return { reducer, initialState };
}
