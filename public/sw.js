importScripts('https://storage.googleapis.com/workbox-cdn/releases/6.0.2/workbox-sw.js');

workbox.setConfig({ debug: false });
workbox.loadModule('workbox-strategies');
workbox.loadModule('workbox-expiration');
// workbox.setConfig({ debug: false });

workbox.routing.registerRoute(
  (obj) => {
    const { url } = obj;
    return url.href.includes('portfolio');
  },
  // new workbox.strategies.NetworkFirst(),
  new workbox.strategies.StaleWhileRevalidate({
    cacheName: 'portfolio-html',
    plugins: [
      new workbox.expiration.ExpirationPlugin({
        // 1 week in seconds
        maxAgeSeconds: 31536000,
      }),
    ],
  }),
);

workbox.routing.registerRoute(
  ({ request }) => request.destination === 'script'
    || request.destination === 'style',
  new workbox.strategies.StaleWhileRevalidate({
    cacheName: 'js-css-assets',
    plugins: [
      new workbox.expiration.ExpirationPlugin({
        maxEntries: 50,
        // 1 week in seconds
        maxAgeSeconds: 31536000,
      }),
    ],
  }),
);

workbox.routing.registerRoute(
  ({ url }) => url.origin === 'https://fonts.googleapis.com'
    || url.origin === 'https://fonts.gstatic.com',
  new workbox.strategies.StaleWhileRevalidate({
    cacheName: 'google-fonts',
    plugins: [
      new workbox.expiration.ExpirationPlugin({
        maxEntries: 20,
        // 1 year in seconds
        maxAgeSeconds: 604800,
      }),
    ],
  }),
);

workbox.routing.registerRoute(
  ({ url }) => url.href === 'https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css',
  new workbox.strategies.StaleWhileRevalidate({
    cacheName: 'animate-css',
    plugins: [
      new workbox.expiration.ExpirationPlugin({
        maxEntries: 20,
        // 1 year in seconds
        maxAgeSeconds: 31536000,
      }),
    ],
  }),
);

workbox.routing.registerRoute(
  ({ url }) => url.href === 'https://unpkg.com/nprogress@0.2.0/nprogress.css',
  new workbox.strategies.StaleWhileRevalidate({
    cacheName: 'nprogress-css',
    plugins: [
      new workbox.expiration.ExpirationPlugin({
        maxEntries: 20,
        // 1 year in seconds
        maxAgeSeconds: 31536000,
      }),
    ],
  }),
);

workbox.routing.registerRoute(
  ({ url }) => url.href.includes('/raymond-profile.jpg'),
  new workbox.strategies.StaleWhileRevalidate({
    cacheName: 'raymond-photo-profile',
    plugins: [
      new workbox.expiration.ExpirationPlugin({
        maxEntries: 20,
        // 1 week in seconds
        maxAgeSeconds: 604800,
      }),
    ],
  }),
);
