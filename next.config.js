// next.config.js
const _ = require('lodash');
const path = require('path');
const withBundleAnalyzer = require('@next/bundle-analyzer')({
  enabled: process.env.ANALYZE === 'true',
});
const webpackConfig = require('./webpack.config');

const wrappers = _.flow(
  withBundleAnalyzer,
);

module.exports = wrappers({
  distDir: process.env.NODE_ENV === 'production' ? './build' : '.next',
  poweredByHeader: false,
  // useFileSystemPublicRoutes: false,
  trailingSlash: false,
  webpack: webpackConfig,
  sassOptions: {
    includePaths: [path.join(__dirname, 'src', 'style')],
  },
});
